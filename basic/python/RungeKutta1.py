#!/usr/bin/env python
# coding: utf-8

# Python program to implement Runge Kutta method
# Markus Schmid
# 2020 Appalachian State University
# jupyter nbconvert --to python FILENAME.ipynb

# A sample differential equation "dy/dt = y"
# Import used libraries
import numpy as np
import math
import matplotlib.pyplot as plt

# list for time (x axis) and result (y axis)
result = []
time = []

# define functions
def dydx(t, y):
    return (y)

# Finds value of y for a given x using step size h
# and initial value y0 at x0.
def rungeKutta4(t, y, t_final, h):
    # Count number of iterations using step size or
    # step height h
    n = (int)((t_final - t)/h)
    for i in range(1, n + 1):
        # Apply Runge Kutta to find next value of y
        k1 = h * dydx(t0, y)
        k2 = h * dydx(t0 + 0.5 * h, y + 0.5 * k1)
        k3 = h * dydx(t0 + 0.5 * h, y + 0.5 * k2)
        k4 = h * dydx(t0 + h, y + k3)

        # Update next value of y
        y = y + (1.0 / 6.0)*(k1 + 2 * k2 + 2 * k3 + k4)

        result.append(y)
        time.append(t)

        # Update next value of t
        t = t + h
    return y

# initial conditions
t0 = 0
y0 = 1

t_final = 5     # final time
h = 0.001         # fixed step size

rungeKutta4(t0, y0, t_final, h)
plt.figure(0)
plt.plot(time,result)
plt.xlabel('x')
plt.ylabel('f (x)')
plt.title("e-Fctn")
plt.grid(True)
plt.savefig('efctn.png', dpi=200)
