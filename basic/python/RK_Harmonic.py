#!/usr/bin/env python
# coding: utf-8

# Python program to implement Runge Kutta method
# Markus Schmid
# 2020 Appalachian State University
# jupyter nbconvert --to python FILENAME.ipynb

# A harmonic oszillator d2y/d2t + sin y = 0
# Import used libraries
import numpy as np
import math
import matplotlib.pyplot as plt

# list for time (x axis) and result (y axis)
result = []
time = []

# define functions
def funcf(t, y, z):
    return (z)

def funcg(t, y, z):
    return (-math.sin(y))

# Finds value of y for a given x using step size h
# and initial value y0 at x0.
def rungeKutta4(t, y, z, t_final, h):
    # Count number of iterations using step size or
    # step height h
    n = (int)((t_final - t)/h)
    for i in range(1, n + 1):
        # Apply Runge Kutta to find next value of y
        k1 = h * funcf(t, y, z)
        l1 = h * funcg(t, y, z)

        k2 = h * funcf(t + 0.5 * h, y + 0.5 * k1, z + 0.5 * l1)
        l2 = h * funcg(t + 0.5 * h, y + 0.5 * k1, z + 0.5 * l1)

        k3 = h * funcf(t + 0.5 * h, y + 0.5 * k2, z + 0.5 * l2)
        l3 = h * funcg(t + 0.5 * h, y + 0.5 * k2, z + 0.5 * l2)

        k4 = h * funcf(t + h, y + k3, z + l3)
        l4 = h * funcg(t + h, y + k3, z + l3)

        # Update next value of y
        y = y + (1.0 / 6.0)*(k1 + 2 * k2 + 2 * k3 + k4)
        z = z + (1.0 / 6.0)*(l1 + 2 * l2 + 2 * l3 + l4)

        result.append(y)
        time.append(t)

        # Update next value of t
        t = t + h
    return y

# initial conditions
t0 = 0
y0 = math.pi/4
z0 = 0
t_final = 4*math.pi     # final time
h = 0.01         # fixed step size

rungeKutta4(t0, y0, z0, t_final, h)
plt.figure(0)
plt.plot(time,result)
plt.xlabel('time')
plt.ylabel('Amplitude')
plt.title("Amplitude over time")
plt.grid(True)
plt.savefig('Ampl.png', dpi=200)
