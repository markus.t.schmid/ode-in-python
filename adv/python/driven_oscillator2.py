#!/usr/bin/env python
# coding: utf-8

# Python program to implement Runge Kutta method
# Markus Schmid
# 2020 Appalachian State University
# jupyter nbconvert --to python FILENAME.ipynb
# using list of lists

# y" + 2*beta*y' + w0*sin(y) = A + B*cos(w*t)
# Import used libraries
import numpy as np
import math
import matplotlib.pyplot as plt
import time
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

# list for time (x axis) and result (y axis)
result = []
w0 = 10                  # undamped angular frequency of the oscillator
beta = 1
B = 1
A = 0
B = 10
w = 4
theta_init = 0
theta_end = 20
theta_step = 0.01

# define functions
def funcf(t, y, z):
    return (z)
def funcg(t, y, z):
    return (A+B*math.cos(w*t) - 2*beta*z - w0*math.sin(y))

# Finds value of y for a given x using step size h
# and initial value y0 at x0.
def rungeKutta4(t, y, z, t_final):
    # Count number of iterations using step size or
    # step height h
    n = (int)((t_final - t)/h)
    for i in range(1, n + 1):
        # Apply Runge Kutta to find next value of y
        k1 = h * funcf(t, y, z)
        l1 = h * funcg(t, y, z)

        k2 = h * funcf(t + 0.5 * h, y + 0.5 * k1, z + 0.5 * l1)
        l2 = h * funcg(t + 0.5 * h, y + 0.5 * k1, z + 0.5 * l1)

        k3 = h * funcf(t + 0.5 * h, y + 0.5 * k2, z + 0.5 * l2)
        l3 = h * funcg(t + 0.5 * h, y + 0.5 * k2, z + 0.5 * l2)

        k4 = h * funcf(t + h, y + k3, z + l3)
        l4 = h * funcg(t + h, y + k3, z + l3)

        # Update next value of y
        y = y + (1.0 / 6.0)*(k1 + 2 * k2 + 2 * k3 + k4)
        z = z + (1.0 / 6.0)*(l1 + 2 * l2 + 2 * l3 + l4)

        result.append(y)
        t = t + h # Update next value of t
    return y

# initial conditions
t0 = 0
y0 = 0
z0 = 0
t_final = 20                   # final time
h = 0.01                    # fixed step size
n = (int)((t_final - t0)/h)   # datapoints per RK4 iteration

iterations = np.arange(theta_init,theta_end+theta_step,theta_step)   # number iterations for omega sweep
start_time = time.time()
for k in iterations:
            rungeKutta4(t0, k, z0, t_final)

end_time = time.time()
print ("The program took", end_time - start_time, "s to run")

table = np.array(result).reshape(len(iterations),n)   # rearrange array, 1 row is const. omega
timer = np.arange(t0,t_final,h) # time array

#A_w = np.amax(table, axis=1)  # max value of each row with axis = 1 , axis=0 gives max of each collumn
sub_table = table[::,int(len(timer)/2):] # only take the 2nd half of the array after the transient state
A_w = np.amax(sub_table, axis=1)  # max value of each row with axis = 1 , axis=0 gives max of each collumn
plt.figure(0)
plt.plot(iterations,A_w)
plt.xlabel('$\\theta_0$')
plt.ylabel('Amplitude')
plt.title("A($\\theta_0$)")
plt.grid(True)
plt.savefig('Ampl_as_fct_theta0.png', dpi=200)

# Plot for a specific theta_0 -- choose array index
index = 1
theta = index * theta_step
plt.figure(1)
plt.plot(timer,table[index,:])
plt.xlabel('time')
plt.ylabel('Amplitude')
plt.title("A(t) at $\\theta_0$ = %.4f" %theta)
plt.grid(True)
plt.savefig('Ampl_of_index.png', dpi=200)
