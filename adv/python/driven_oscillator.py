#!/usr/bin/env python
# coding: utf-8

# Python program to implement Runge Kutta method
# Markus Schmid
# 2020 Appalachian State University
# jupyter nbconvert --to python FILENAME.ipynb

# A harmonic oszillator d2y/d2t + 2*B*dy/dt +w0*w0*y= F * cos(w*t)
# Import used libraries
import numpy as np
import math
import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

# list for time (x axis) and result (y axis)
result = []
w0 = 10                  # undamped angular frequency of the oscillator
B = 1
F = 10
w_init = 1
w_end = 20
w_step = 0.1

# define functions
def funcf(t, y, z):
    return (z)
def funcg(t, y, z):
    return (F*math.cos(w*t)- 2*B*z - w0*w0*y)

# Finds value of y for a given x using step size h
# and initial value y0 at x0.
def rungeKutta4(t, y, z, t_final, h):
    # Count number of iterations using step size or
    # step height h
    n = (int)((t_final - t)/h)
    for i in range(1, n + 1):
        # Apply Runge Kutta to find next value of y
        k1 = h * funcf(t, y, z)
        l1 = h * funcg(t, y, z)

        k2 = h * funcf(t + 0.5 * h, y + 0.5 * k1, z + 0.5 * l1)
        l2 = h * funcg(t + 0.5 * h, y + 0.5 * k1, z + 0.5 * l1)

        k3 = h * funcf(t + 0.5 * h, y + 0.5 * k2, z + 0.5 * l2)
        l3 = h * funcg(t + 0.5 * h, y + 0.5 * k2, z + 0.5 * l2)

        k4 = h * funcf(t + h, y + k3, z + l3)
        l4 = h * funcg(t + h, y + k3, z + l3)

        # Update next value of y
        y = y + (1.0 / 6.0)*(k1 + 2 * k2 + 2 * k3 + k4)
        z = z + (1.0 / 6.0)*(l1 + 2 * l2 + 2 * l3 + l4)

        result.append(y)
        t = t + h # Update next value of t
    return y

# initial conditions
t0 = 0
y0 = 0
z0 = 0
t_final = 20                   # final time
h = 0.01                    # fixed step size
n = (int)((t_final - t0)/h)   # datapoints per RK4 iteration

iterations = np.arange(w_init,w_end+w_step,w_step)   # number iterations for omega sweep
w = w_init
for k in iterations:
    rungeKutta4(t0, y0, z0, t_final, h)
    w = w + w_step

table = np.array(result).reshape(len(iterations),n)   # rearrange array, 1 row is const. omega
timer = np.arange(t0,t_final,h) # time array

#A_w = np.amax(table, axis=1)  # max value of each row with axis = 1 , axis=0 gives max of each collumn
sub_table = table[::,int(len(timer)/2):]
A_w = np.amax(sub_table, axis=1)  # max value of each row with axis = 1 , axis=0 gives max of each collumn
plt.figure(0)
plt.plot(iterations,A_w)
plt.xlabel('$\omega$')
plt.ylabel('Amplitude')
plt.title("A($\omega$)")
plt.grid(True)
plt.savefig('A_vs_o.png', dpi=200)

# Plot for a specific omega -- choose array index
index = 1
omega = 1+(index * w_step)
plt.figure(1)
plt.plot(timer,table[index,:])
plt.xlabel('time')
plt.ylabel('Amplitude')
plt.title("A(t) at $\omega$ = %.1f" %omega)
plt.grid(True)
plt.savefig('A_index', dpi=200)

# analytic solution
calc = []
for k in iterations:
    temp = F /math.sqrt((w0*w0-k*k)**2+4*B*B*k*k)
    calc.append(temp)
plt.figure(2)
plt.plot(iterations,calc)
plt.xlabel('$\omega$')
plt.ylabel('Amplitude')
plt.title("Analytic solution A($\omega$)")
plt.grid(True)
plt.savefig('Analytic_solution', dpi=200)
